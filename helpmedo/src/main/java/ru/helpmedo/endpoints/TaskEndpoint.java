package ru.helpmedo.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.helpmedo.domains.DirectoryTaskStatuses;
import ru.helpmedo.domains.User;
import ru.helpmedo.domains.UserTask;
import ru.helpmedo.repos.UserTasksRepo;
import ru.helpmedo.util.CurrentUser;

import javax.servlet.http.HttpServletRequest;
import java.time.ZonedDateTime;
import java.util.HashMap;

@RestController
@RequestMapping("/rtask")
public class TaskEndpoint {

    @Autowired
    private UserTasksRepo userTasksRepo;

    private User currentUser = null;

    private UserTask userTask = null;

    @GetMapping("{idTask}")
    public HashMap<String, String> getTask(HttpServletRequest httpServletRequest,
                                           @PathVariable("idTask") Long idTask) {

        currentUser = CurrentUser.getCurrentUser(httpServletRequest);
        userTask = (UserTask) userTasksRepo.findByIdUserTasksEquals(idTask);

        if (checkTaskPermissionAndNull()) {
            return new HashMap<String, String>() {{
                put("method", "GET");
                put("status", "OK");
                put("id_user_tasks", userTask.getIdUserTasks().toString());
                put("id_user", userTask.getIdUser().toString());
                put("date_of_creation", userTask.getDateOfCreation());
                put("date_of_creation_zdt", userTask.getDateOfCreationZoneDateTime());
                put("task_name", userTask.getTaskName());
                put("id_status", userTask.getDirectoryTaskStatuses().getId().toString());
                put("status_name", userTask.getDirectoryTaskStatuses().getNameStatus());
                put("closing_date", userTask.getClosingDate());
                put("closing_date_zdt", userTask.getClosingDateZoneDateTime());
            }};
        }

        return new HashMap<String, String>() {{
            put("method", "GET");
            put("status", "FAIL");
            put("cause_of_error", "The user doesn't have such the task!");
        }};
    }

    @PutMapping
    public HashMap<String, String> editTask(HttpServletRequest httpServletRequest,
                                            @RequestParam("idTask") Long idTask,
                                            @RequestParam("taskName") String taskName,
                                            @RequestParam("idStatus") Long idStatus,
                                            @RequestParam(value = "closingDate", required = false) String closingDate) {

        currentUser = CurrentUser.getCurrentUser(httpServletRequest);
        userTask = userTasksRepo.findByIdUserTasksEquals(idTask);

        if (checkTaskPermissionAndNull()) {
            userTask.setTaskName(taskName);
            if (closingDate != null && !closingDate.isEmpty()) {
                userTask.setClosingDate(ZonedDateTime.parse(closingDate));
            }

            DirectoryTaskStatuses dts = new DirectoryTaskStatuses();
            dts.setId(idStatus);

            userTask.setDirectoryTaskStatuses(dts);
            userTask = userTasksRepo.save(userTask);

            return new HashMap<String, String>() {{
                put("method", "PUT");
                put("status", "OK");
            }};
        }
        return new HashMap<String, String>() {{
            put("method", "PUT");
            put("status", "FAIL");
            put("cause_of_error", "The user doesn't have such the task!");
        }};
    }

    /*@PostMapping("{taskName}")
    public HashMap<String, String> createTaskUser(HttpServletRequest httpServletRequest,
                                                  @PathVariable("taskName") String taskName) {

        currentUser = CurrentUser.getCurrentUser(httpServletRequest);
        DirectoryTaskStatuses dts = new DirectoryTaskStatuses();
        dts.setId((long) 1);
        UserTask newUserTask =
                new UserTask(currentUser.getId_user(), ZonedDateTime.now(), dts, taskName);
        userTask = userTasksRepo.save(newUserTask);
        if (userTask == null) {
            return new HashMap<String, String>() {{
                put("method", "POST");
                put("status", "FAIL");
                put("cause_of_error", "Task fail!");
            }};
        }
        return new HashMap<String, String>() {{
            put("method", "POST");
            put("status", "OK");
            put("id_user_task", userTask.getIdUserTasks().toString());
        }};
    }*/

    @DeleteMapping("{idTask}")
    public HashMap<String, String> deleteTaskUser(HttpServletRequest httpServletRequest,
                                                  @PathVariable("idTask") Long idTask) {
        currentUser = CurrentUser.getCurrentUser(httpServletRequest);
        userTask = (UserTask) userTasksRepo.findByIdUserTasksEquals(idTask);

        if (checkTaskPermissionAndNull()) {
            userTasksRepo.deleteById(idTask);
            return new HashMap<String, String>() {{
                put("method", "DELETE");
                put("status", "OK");
            }};
        }
        return new HashMap<String, String>() {{
            put("method", "DELETE");
            put("status", "FAIL");
            put("cause_of_error", "The user doesn't have such the task!");
        }};
    }

    private boolean checkTaskPermissionAndNull() {
        return userTask != null && currentUser.getId_user().equals(userTask.getIdUser());
    }
}