package ru.helpmedo.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

@Entity
@Table(name = "user_tasks")
public class UserTask implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user_tasks")
    private Long idUserTasks;

    @Column(name = "id_user")
    private Long idUser;

    @Column(name = "date_of_creation")
    private ZonedDateTime dateOfCreation;

    @Column(name = "task_name")
    private String taskName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_directory_task_statuses", referencedColumnName = "id")
    private DirectoryTaskStatuses directoryTaskStatuses;

    @Column(name = "closing_date")
    private ZonedDateTime closingDate;

    public UserTask() {

    }

    public UserTask(HashMap<String, String> response) {
        this.idUser = Long.valueOf(response.get("id_user"));
        this.idUserTasks = Long.valueOf(response.get("id_user_tasks"));
        if (!response.get("date_of_creation_zdt").equals("")) {
            this.dateOfCreation = ZonedDateTime.parse(response.get("date_of_creation_zdt"));
        } else {
            this.dateOfCreation = null;
        }
        this.taskName = response.get("task_name");
        DirectoryTaskStatuses dts = new DirectoryTaskStatuses();
        dts.setId(Long.valueOf(response.get("id_status")));
        dts.setNameStatus(response.get("status_name"));
        this.setDirectoryTaskStatuses(dts);
        if (!response.get("closing_date_zdt").equals("")) {
            this.closingDate = ZonedDateTime.parse(response.get("closing_date_zdt"));
        } else {
            this.closingDate = null;
        }
    }

    public UserTask(Long idUser, ZonedDateTime dateOfCreation, DirectoryTaskStatuses directoryTaskStatuses, String taskName) {
        this.idUser = idUser;
        this.dateOfCreation = dateOfCreation;
        this.taskName = taskName;
        this.directoryTaskStatuses = directoryTaskStatuses;
        this.closingDate = closingDate;
    }

    public Long getIdUserTasks() {
        return idUserTasks;
    }

    public void setIdUserTasks(Long idUserTasks) {
        this.idUserTasks = idUserTasks;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getDateOfCreation() {
        return dateOfCreation == null ? "" : DateTimeFormatter.ofPattern("dd-MM-yyyy - hh:mm").format(dateOfCreation);
    }

    public void setDateOfCreation(ZonedDateTime dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public DirectoryTaskStatuses getDirectoryTaskStatuses() {
        return directoryTaskStatuses;
    }

    public void setDirectoryTaskStatuses(DirectoryTaskStatuses directoryTaskStatuses) {
        this.directoryTaskStatuses = directoryTaskStatuses;
    }

    public String getClosingDate() {
        return closingDate == null ? "" : DateTimeFormatter.ofPattern("dd-MM-yyyy - hh:mm").format(closingDate);
    }

    public void setClosingDate(ZonedDateTime closingDate) {
        this.closingDate = closingDate;
    }

    public String getDateOfCreationZoneDateTime() {
        return dateOfCreation == null ? "" : dateOfCreation.toString();
    }

    public String getClosingDateZoneDateTime() {
        return closingDate == null ? "" : closingDate.toString();
    }
}