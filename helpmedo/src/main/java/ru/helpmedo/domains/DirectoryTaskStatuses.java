package ru.helpmedo.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "directory_task_statuses")
public class DirectoryTaskStatuses implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name_status")
    private String nameStatus;

    @OneToMany(mappedBy = "directoryTaskStatuses")
    private List<UserTask> userTask;


    public DirectoryTaskStatuses() {

    }

    public DirectoryTaskStatuses(String nameStatus, List<UserTask> userTask) {
        this.nameStatus = nameStatus;
        this.userTask = userTask;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public List<UserTask> getUserTask() {
        return userTask;
    }

    public void setUserTask(List<UserTask> userTask) {
        this.userTask = userTask;
    }
}