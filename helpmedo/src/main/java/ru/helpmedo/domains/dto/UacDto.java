package ru.helpmedo.domains.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UacDto {
    private String id_user;
    private String login;
    private String first_name;
    private String last_name;
    private String registration_date;

    public UacDto(String id_user, String login, String first_name, String last_name, String registration_date) {
        this.id_user = id_user;
        this.login = login;
        this.first_name = first_name;
        this.last_name = last_name;
        this.registration_date = registration_date;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }
}
