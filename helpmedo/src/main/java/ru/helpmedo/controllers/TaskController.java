package ru.helpmedo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.helpmedo.domains.UserTask;
import ru.helpmedo.endpoints.TaskEndpoint;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Autowired
    private MainController mainController;

    @GetMapping("{idTask}")
    public String getTask(HttpServletRequest httpServletRequest,
                          Model model,
                          @PathVariable("idTask") Long idTask) {

        HashMap<String, String> response = taskEndpoint.getTask(httpServletRequest, idTask);

        if ("OK".equalsIgnoreCase(response.get("status"))) {
            model.addAttribute("usertask", new UserTask(response));
        } else {
            return "error/404";
        }

        return "task";
    }

    @PostMapping("/delete/{idTask}")
    public String deleteTask(HttpServletRequest httpServletRequest,
                             Model model,
                             @PathVariable("idTask") Long idTask) {

        HashMap<String, String> response = taskEndpoint.deleteTaskUser(httpServletRequest, idTask);

        if ("FAIL".equalsIgnoreCase(response.get("status"))) {
            return "error/404";
        }

        return mainController.getMainPage(httpServletRequest, model, 0);
    }

    @PostMapping("/edit")
    public String editTask(HttpServletRequest httpServletRequest,
                           Model model,
                           @RequestParam("idTask") Long idTask,
                           @RequestParam("taskName") String taskName,
                           @RequestParam("idStatus") Long idStatus,
                           @RequestParam("closingDate") String closingDate) {

        HashMap<String, String> response = taskEndpoint.editTask(httpServletRequest, idTask, taskName, idStatus,
                closingDate);

        if ("FAIL".equalsIgnoreCase(response.get("status"))) {
            return "error/404";
        }

        return this.getTask(httpServletRequest, model, idTask);
    }
}
