package ru.helpmedo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pa")
public class PersonalAreaController {
    @GetMapping
    public String getPersonalAreaPage(){
        return "pa";
    }
}
