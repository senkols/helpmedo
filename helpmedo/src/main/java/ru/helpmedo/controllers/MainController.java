package ru.helpmedo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import ru.helpmedo.domains.User;
import ru.helpmedo.domains.UserTask;
import ru.helpmedo.exceptions.CurrentUserError;
import ru.helpmedo.repos.UserRepo;
import ru.helpmedo.repos.UserTasksRepo;
import ru.helpmedo.util.PaginationBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
public class MainController {
    @Autowired
    private UserTasksRepo userTasksRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${pagination.number_of_records_shown}")
    private Integer countRecordsShown;

    @GetMapping("/main")
    public void getMainWithoutParams(HttpServletRequest httpServletRequest, Model model) {
        this.getMainPage(httpServletRequest, model, 0);
    }

    @GetMapping("/main/{page}")
    public String getMainPage(HttpServletRequest httpServletRequest, Model model,
                              @PathVariable("page") Integer page) {

        User currentUser = setUserAttributeInCurrentSession(httpServletRequest);
        Pageable pageable = setPaginationPropertiesCurrentUser(httpServletRequest, model, page,
                currentUser);
        model.addAttribute("listUserTasks", getCurrentUserTaskList(currentUser, pageable));
        return "main";
    }

    private List<List<UserTask>> getCurrentUserTaskList(User currentUser, Pageable pageable) {

        List<UserTask> userTasks = userTasksRepo.
                findAllByIdUserEqualsOrderByIdUserTasksAscDateOfCreationAsc(currentUser.getId_user(),
                        pageable);
        List<List<UserTask>> columnUserTaskSheet = new ArrayList<>();
        for (int i = 0; i < userTasks.size() - 1; i += 2) {
            List<UserTask> twoTasks = new ArrayList<>();
            twoTasks.add(userTasks.get(i));
            twoTasks.add(userTasks.get(i + 1));
            columnUserTaskSheet.add(twoTasks);
        }
        return columnUserTaskSheet;
    }

    private Pageable setPaginationPropertiesCurrentUser(HttpServletRequest httpServletRequest, Model model, @PathVariable("page") Integer page, User currentUser) {
        Pageable pageable = new PageRequest(page, countRecordsShown);
        model.addAttribute("pagination",
                new PaginationBuilder(httpServletRequest,
                        userTasksRepo.countAllByIdUserEquals(currentUser.getId_user()), page,
                        countRecordsShown));
        return pageable;
    }

    private User setUserAttributeInCurrentSession(HttpServletRequest httpServletRequest) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User currentUser = null;
        if (principal instanceof UserDetails) {
            currentUser = userRepo.findByLoginEquals(((UserDetails) principal).getUsername());
        } else {
            throw new CurrentUserError("Ошибка получения текущего пользователя!");
        }

        httpServletRequest.
                getSession().
                setAttribute("user" +
                        httpServletRequest.getSession().getId(), currentUser);
        return currentUser;
    }
}
