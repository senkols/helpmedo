package ru.helpmedo.repos;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import ru.helpmedo.domains.UserTask;

import java.util.List;

public interface UserTasksRepo extends CrudRepository<UserTask, Long> {
    public List<UserTask> findAllByIdUserEqualsOrderByIdUserTasksAscDateOfCreationAsc(Long idUser, Pageable pageable);

    public Integer countAllByIdUserEquals(Long idUser);

    public UserTask findByIdUserTasksEquals(Long idUserTasks);

    public UserTask findByIdUserAndIdUserTasks(Long idUser, Long idUserTasks);
}
