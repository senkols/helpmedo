package ru.helpmedo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class CurrentUserError extends RuntimeException {
    private String message;

    public CurrentUserError(String errorMessage) {
        message = errorMessage;
    }

    @Override
    public String toString() {
        return "CurrentUserError{" +
                "message='" + message + '\'' +
                '}';
    }
}
