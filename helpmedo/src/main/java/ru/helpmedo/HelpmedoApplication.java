package ru.helpmedo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpmedoApplication {
    public static void main(String[] args) {
        SpringApplication.run(HelpmedoApplication.class, args);
    }
}

