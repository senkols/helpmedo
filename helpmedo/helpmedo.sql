﻿/* SQL Manager for MySQL                              5.7.2.52112 */
/* -------------------------------------------------------------- */
/* Host     : localhost                                           */
/* Port     : 3306                                                */
/* Database : helpmedo                                            */


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES 'utf8' */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `helpmedo`;

CREATE DATABASE `helpmedo`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `helpmedo`;

/* Удаление объектов БД */

DROP TABLE IF EXISTS `user_tasks`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `directory_task_statuses`;

/* Структура для таблицы `directory_task_statuses`:  */

CREATE TABLE `directory_task_statuses` (
  `id` INTEGER(11) NOT NULL AUTO_INCREMENT COMMENT 'Суррогатный идентификатор статуса задачи',
  `name_status` VARCHAR(150) COLLATE utf8_general_ci NOT NULL COMMENT 'Наименование статуса',
  PRIMARY KEY USING BTREE (`id`),
  UNIQUE KEY `id_directory_task_statuses` USING BTREE (`id`),
  UNIQUE KEY `name_status` USING BTREE (`name_status`)
) ENGINE=InnoDB
AUTO_INCREMENT=12 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Справочник статусов задачи'
;

/* Структура для таблицы `users`:  */

CREATE TABLE `users` (
  `id_user` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор пользователя',
  `login` VARCHAR(80) COLLATE utf8_general_ci NOT NULL COMMENT 'Логин пользователя (email)',
  `first_name` VARCHAR(80) COLLATE utf8_general_ci NOT NULL COMMENT 'Имя пользователя',
  `last_name` VARCHAR(80) COLLATE utf8_general_ci NOT NULL COMMENT 'Фамилия пользователя',
  `patronomic` VARCHAR(80) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Отчество пользователя',
  `password` VARCHAR(80) COLLATE utf8_general_ci NOT NULL COMMENT 'Пароль пользователя',
  `active` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Признак подтверждения учетной записи',
  `token` VARCHAR(100) COLLATE utf8_general_ci DEFAULT NULL COMMENT 'Идентификатор для подтверждения аккаунта',
  `registration_date` DATETIME NOT NULL COMMENT 'Дата и время регистрации',
  PRIMARY KEY USING BTREE (`id_user`),
  UNIQUE KEY `id_user` USING BTREE (`id_user`),
  UNIQUE KEY `login` USING BTREE (`login`),
  UNIQUE KEY `token` USING BTREE (`token`)
) ENGINE=InnoDB
AUTO_INCREMENT=33 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Таблица пользователей системы'
;

/* Структура для таблицы `user_tasks`:  */

CREATE TABLE `user_tasks` (
  `id_user_tasks` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT 'Суррогатный идентификатор задачи',
  `id_user` BIGINT(20) NOT NULL COMMENT 'Первичный ключ (идентификатор пользователя)',
  `date_of_creation` DATETIME NOT NULL COMMENT 'Дата создания задачи',
  `task_name` VARCHAR(253) COLLATE utf8_general_ci NOT NULL COMMENT 'Наименование задачи',
  `id_directory_task_statuses` INTEGER(11) NOT NULL DEFAULT 1 COMMENT 'Первичный ключ (идентификатор статуса задачи)',
  `closing_date` DATETIME DEFAULT NULL COMMENT 'Дата закрытия задачи',
  PRIMARY KEY USING BTREE (`id_user_tasks`),
  UNIQUE KEY `id_user_tasks` USING BTREE (`id_user_tasks`),
  KEY `user_tasks_Id_user_users` USING BTREE (`id_user`),
  KEY `user_tasks_fk1` USING BTREE (`id_directory_task_statuses`),
  CONSTRAINT `user_tasks_Id_user_users` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_tasks_fk1` FOREIGN KEY (`id_directory_task_statuses`) REFERENCES `directory_task_statuses` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB
AUTO_INCREMENT=8 ROW_FORMAT=DYNAMIC CHARACTER SET 'utf8' COLLATE 'utf8_general_ci'
COMMENT='Задачи пользователей'
;

/* Data for the `directory_task_statuses` table  (LIMIT 0,500) */

INSERT INTO `directory_task_statuses` (`id`, `name_status`) VALUES
  (1,'Зарегистрирован'),
  (2,'Открыт'),
  (3,'Анализ'),
  (4,'В работе'),
  (5,'Тестирование'),
  (6,'Сделано'),
  (7,'Закрыт'),
  (8,'В ожидании'),
  (9,'Сделать'),
  (10,'Проверено'),
  (11,'Заблокирована');
COMMIT;

/* Data for the `users` table  (LIMIT 0,500) */

INSERT INTO `users` (`id_user`, `login`, `first_name`, `last_name`, `patronomic`, `password`, `active`, `token`, `registration_date`) VALUES
  (31,'senko.leonid@gmail.com','Leonid','Senko',NULL,'$2a$12$7KYtt4HBIvUUKcOuOt7x/.ZLSQ.3JLH5zcazl2Rrx.o/ASMv36G/6',1,'$2a$12$XcuqNIt57tuHDne9larXRu067zwJ05p7hHo361M0On7y2yRuXKhJe','2019-01-29 09:04:44'),
  (32,'sdc','sdc','sdcsdc',NULL,'werfwef',0,NULL,'2019-02-15 00:00:00');
COMMIT;

/* Data for the `user_tasks` table  (LIMIT 0,500) */

INSERT INTO `user_tasks` (`id_user_tasks`, `id_user`, `date_of_creation`, `task_name`, `id_directory_task_statuses`, `closing_date`) VALUES
  (6,31,'2019-05-27 21:12:04','Тестовая задача №1',1,'2019-05-27 00:00:00'),
  (7,31,'2019-05-27 00:00:00','Тестовая задача 2',2,'2019-05-27 00:00:00');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;